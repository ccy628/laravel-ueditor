@if (!defined('UEDITOR_JS'))
    <script src="{!!asset('/static/laravel-ueditor/ueditor.config.js')!!}"></script>
    <script src="{!!asset('/static/laravel-ueditor/ueditor.all.min.js')!!}"></script>
    {{-- 载入语言文件,根据laravel的语言设置自动载入 --}}
    <script src="{!!asset($UeditorLangFile)!!}"></script>
    @php
        define('UEDITOR_JS', 1);
    @endphp
@endif


<!-- 加载编辑器的容器 -->
<script id="{{$name ?? 'container'}}" name="{{$name ?? 'container'}}" type="text/plain">{!! $content ?? '' !!}</script>
<!-- 实例化编辑器 -->
<script type="text/javascript">
    var ue = UE.getEditor('{{$name ?? 'container'}}',{
        initialFrameWidth: null,
        initialFrameHeight: 250,
    });
    ue.ready(function() {
        ue.execCommand('serverparam', '_token', '{{ csrf_token() }}');
    });
</script>
